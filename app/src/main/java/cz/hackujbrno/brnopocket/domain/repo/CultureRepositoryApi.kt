package cz.hackujbrno.brnopocket.domain.repo

import cz.hackujbrno.brnopocket.domain.models.Event

/**
 * Created by Petra Cendelínová on 09,April,2022
 * cendpetra@gmail.com
 */
interface CultureRepositoryApi {

    suspend fun getEvents(): Result<List<Event>>
}