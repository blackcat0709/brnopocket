package cz.hackujbrno.brnopocket.domain.usecases

import cz.hackujbrno.brnopocket.domain.models.WasteYard
import cz.hackujbrno.brnopocket.domain.repo.PlacesRepositoryApi

/**
 * Created by Petra Cendelínová on 10,April,2022
 * cendpetra@gmail.com
 */
class GetWasteYardsUseCase(private val placesRepository: PlacesRepositoryApi) :
    BaseOutputUseCase<Result<List<WasteYard>>>() {

    override suspend fun execute(): Result<List<WasteYard>> = placesRepository.getWasteYards()
}