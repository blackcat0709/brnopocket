package cz.hackujbrno.brnopocket.domain.mappers

import cz.hackujbrno.brnopocket.data.remote.models.events.EventResponse
import cz.hackujbrno.brnopocket.domain.models.Event

/**
 * Created by Petra Cendelínová on 09,April,2022
 * cendpetra@gmail.com
 */

fun EventResponse.toEvent(): Event = Event(
    id = id,
    name = name,
    description = description ?: "",
    tickets = tickets?.replace(",","\n") ?: "",
    ticketsInfo = ticketsInfo ?: "",
    imageUrl = imageUrl,
    url = url,
    categories = categories ?: "",
    organizerEmail = organizerEmail ?: "",
    dateFrom = dateFrom,
    dateTo = dateTo,
    latitude = latitude,
    longitude = longitude
)