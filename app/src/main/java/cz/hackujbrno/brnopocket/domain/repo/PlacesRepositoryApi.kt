package cz.hackujbrno.brnopocket.domain.repo

import cz.hackujbrno.brnopocket.domain.models.WasteYard

/**
 * Created by Petra Cendelínová on 10,April,2022
 * cendpetra@gmail.com
 */
interface PlacesRepositoryApi {

    suspend fun getWasteYards(): Result<List<WasteYard>>
}