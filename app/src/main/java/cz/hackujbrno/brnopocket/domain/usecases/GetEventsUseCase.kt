package cz.hackujbrno.brnopocket.domain.usecases

import cz.hackujbrno.brnopocket.domain.models.Event
import cz.hackujbrno.brnopocket.domain.repo.CultureRepositoryApi

/**
 * Created by Petra Cendelínová on 09,April,2022
 * cendpetra@gmail.com
 */
class GetEventsUseCase(
    private val cultureRepository: CultureRepositoryApi
) : BaseOutputUseCase<Result<List<Event>>>() {
    override suspend fun execute(): Result<List<Event>> = cultureRepository.getEvents()
}