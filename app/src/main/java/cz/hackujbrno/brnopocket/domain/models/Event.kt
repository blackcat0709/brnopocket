package cz.hackujbrno.brnopocket.domain.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * Created by Petra Cendelínová on 09,April,2022
 * cendpetra@gmail.com
 */

@Parcelize
data class Event(
    val id: Long,
    val name: String,
    val description: String,
    val tickets: String,
    val ticketsInfo: String,
    val imageUrl: String,
    val url: String,
    val categories: String,
    val organizerEmail: String,
    val dateFrom: Long,
    val dateTo: Long,
    val latitude: Double,
    val longitude: Double
): Parcelable