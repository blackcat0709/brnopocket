package cz.hackujbrno.brnopocket.domain.models

/**
 * Created by Petra Cendelínová on 10,April,2022
 * cendpetra@gmail.com
 */
data class WasteYard(
    val id: Long,
    val name: String,
    val x: Double,
    val y: Double
)
