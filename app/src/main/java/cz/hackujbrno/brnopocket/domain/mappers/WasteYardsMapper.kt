package cz.hackujbrno.brnopocket.domain.mappers

import cz.hackujbrno.brnopocket.data.remote.models.wasteYards.WasteYardResponse
import cz.hackujbrno.brnopocket.domain.models.WasteYard

/**
 * Created by Petra Cendelínová on 10,April,2022
 * cendpetra@gmail.com
 */
fun WasteYardResponse.toWasteYard(x: Double, y: Double): WasteYard = WasteYard(
    id = id,
    name = name,
    x = x,
    y = y
)