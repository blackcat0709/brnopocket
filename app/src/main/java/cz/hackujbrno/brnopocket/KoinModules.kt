package cz.hackujbrno.brnopocket

import com.google.gson.Gson
import cz.hackujbrno.brnopocket.data.remote.services.EventsApi
import cz.hackujbrno.brnopocket.data.remote.services.WasteYardsApi
import cz.hackujbrno.brnopocket.data.repo.CultureRepositoryImpl
import cz.hackujbrno.brnopocket.data.repo.PlacesRepositoryImpl
import cz.hackujbrno.brnopocket.domain.repo.CultureRepositoryApi
import cz.hackujbrno.brnopocket.domain.repo.PlacesRepositoryApi
import cz.hackujbrno.brnopocket.domain.usecases.GetEventsUseCase
import cz.hackujbrno.brnopocket.domain.usecases.GetWasteYardsUseCase
import cz.hackujbrno.brnopocket.presentation.ui.events.EventsViewModel
import cz.hackujbrno.brnopocket.presentation.ui.map.MapViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

fun viewModelsModule(): Module = module {
    viewModel { EventsViewModel(get()) }
    viewModel { MapViewModel(get()) }
}

fun domainModule(): Module = module {
    single { GetEventsUseCase(get()) }
    single { GetWasteYardsUseCase(get()) }
}

fun dataModule(): Module = module {
    single<CultureRepositoryApi> { CultureRepositoryImpl(get()) }
    single<PlacesRepositoryApi> { PlacesRepositoryImpl(get()) }

    single {
        val interceptor = HttpLoggingInterceptor().apply {
            setLevel(HttpLoggingInterceptor.Level.BODY)
        }

        OkHttpClient.Builder().addInterceptor(interceptor).build()
    }

    single {
        Retrofit.Builder()
            .baseUrl("https://services6.arcgis.com/fUWVlHWZNxUvTUh8/arcgis/rest/services/")
            .client(OkHttpClient.Builder().build())
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .build()
    }

    single {
        val retrofit = get<Retrofit>()
        retrofit.create(EventsApi::class.java)
    }

    single {
        val retrofit = get<Retrofit>()
        retrofit.create(WasteYardsApi::class.java)
    }
}