package cz.hackujbrno.brnopocket.presentation.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI.setupWithNavController
import cz.hackujbrno.brnopocket.R
import cz.hackujbrno.brnopocket.databinding.ActivityMainBinding


class MainActivity: AppCompatActivity() {

    private lateinit var viewBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        installSplashScreen()
        super.onCreate(savedInstanceState)

        viewBinding = (DataBindingUtil.setContentView(
            this,
            R.layout.activity_main
        ))

        setupBottomNavigation()
    }

    private fun setupBottomNavigation() {
        val navController = Navigation.findNavController(
            this,
            R.id.activity_main_nav_host_fragment
        )
        setupWithNavController(viewBinding.bottomNavigation, navController)
    }
}