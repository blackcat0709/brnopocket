package cz.hackujbrno.brnopocket.presentation.ui.chatbot

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.applozic.mobicommons.commons.core.utils.Utils
import cz.hackujbrno.brnopocket.databinding.FragmentChatbotBinding
import io.kommunicate.KmConversationBuilder
import io.kommunicate.KmConversationHelper
import io.kommunicate.Kommunicate
import io.kommunicate.callbacks.KmCallback


/**
 * Created by Petra Cendelínová on 09,April,2022
 * cendpetra@gmail.com
 */
class ChatbotFragment : Fragment() {

    private lateinit var viewBinding: FragmentChatbotBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentChatbotBinding.inflate(layoutInflater)

        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Kommunicate.openConversation(requireContext());
        KmConversationBuilder(requireContext())
            .createConversation(object : KmCallback {
                override fun onSuccess(message: Any) {
                    val conversationId = message.toString()
                }

                override fun onFailure(error: Any) {
                    Log.d("ConversationTest", "Error : $error")
                }
            })
        }

}