package cz.hackujbrno.brnopocket.presentation.customViews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import cz.hackujbrno.brnopocket.R
import cz.hackujbrno.brnopocket.databinding.ViewMapCategoryTypeBinding
import timber.log.Timber

/**
 * Created by Petra Cendelínová on 10,April,2022
 * cendpetra@gmail.com
 */
class MapCategoryTypeView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val views = ViewMapCategoryTypeBinding.inflate(
        LayoutInflater.from(context),
        this,
        true
    )

    init {
        attrs?.let {
            val typedArray =
                context.obtainStyledAttributes(it, R.styleable.MapCategoryTypeView)
            try {
                with(views) {
//                    cardView.setStrokeColor(Color)
                    categoryName.text =
                        typedArray.getString(R.styleable.MapCategoryTypeView_titleRes)
                    categoryIcon.setImageDrawable(
                        typedArray.getDrawable(R.styleable.MapCategoryTypeView_iconRes)
                    )
                }
            } catch (e: Exception) {
                Timber.w("Failed setup attrs at: $e")
            }
            typedArray.recycle()
        }

    }
}
