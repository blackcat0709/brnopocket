package cz.hackujbrno.brnopocket.presentation.ui.map

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import cz.hackujbrno.brnopocket.R
import cz.hackujbrno.brnopocket.databinding.FragmentMapBinding
import cz.hackujbrno.brnopocket.domain.models.WasteYard
import cz.hackujbrno.brnopocket.presentation.extensions.observe
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Created by Petra Cendelínová on 09,April,2022
 * cendpetra@gmail.com
 */
class MapFragment : Fragment(), OnMapReadyCallback {

    private lateinit var viewBinding: FragmentMapBinding
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>
    private lateinit var map: GoogleMap

    private val mapViewModel: MapViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentMapBinding.inflate(layoutInflater)
//        bindingBottomSheet = BottomSheetBinding.bind(viewBinding.root)

        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

//        bottomSheetBehavior = BottomSheetBehavior.from(bindingBottomSheet.bottomSheet)

        observe(mapViewModel.wasteYardsState) {
            observeMapState(it)
        }
    }

    private fun observeMapState(wasteYardsState: MapViewModel.WasteYardsSte) {
        if (wasteYardsState is MapViewModel.WasteYardsSte.Loaded) {
            with(map) {
                val listOfMarker = mutableListOf<Marker>() // populate this elsewhere
                val builder = LatLngBounds.Builder()
                wasteYardsState.yards.forEach {
                    val latLng = LatLng(it.y, it.x)
                    val marker = addMarker(MarkerOptions().position(latLng).title(it.name))

                    if (marker != null) {
                        listOfMarker.add(marker)
                    }
                    marker?.setIcon(
                        bitmapDescriptorFromVector(
                            requireContext(),
                            R.drawable.ic_waste_marker
                        )
                    )
                }

                for (m in listOfMarker) {
                    builder.include(m.position)
                }

                val paddingFromEdgeAsPX = 100
                val cu = CameraUpdateFactory.newLatLngBounds(builder.build(), paddingFromEdgeAsPX)

                map.animateCamera(cu)
            }

        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        mapViewModel.getWasteYards()
    }

    private fun addMarker(wasteYard: WasteYard) {

    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
        return ContextCompat.getDrawable(context, vectorResId)?.run {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            val bitmap =
                Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888)
            draw(Canvas(bitmap))
            BitmapDescriptorFactory.fromBitmap(bitmap)
        }
    }
}