package cz.hackujbrno.brnopocket.presentation.helpers

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

object TimeHelper {

    private const val TIME_FORMAT_DATE_FULL = "dd/MM/yy" // 24-h format

    @SuppressLint("ConstantLocale")
    private val LOCALE = Locale.getDefault()
    private val TIME_ZONE = TimeZone.getTimeZone("UTC") // standard world's time ignoring time-zones

    private val dateFullFormatter by lazy {
        SimpleDateFormat(
            TIME_FORMAT_DATE_FULL,
            LOCALE
        )
    }

    fun formatDateTime(
        timeInMillis: Long,
    ): String = dateFullFormatter.format(timeInMillis)

}