package cz.hackujbrno.brnopocket.presentation.ui.events

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import cz.hackujbrno.brnopocket.R
import cz.hackujbrno.brnopocket.databinding.ItemEventBinding
import cz.hackujbrno.brnopocket.domain.models.Event

/**
 * Created by Petra Cendelínová on 09,April,2022
 * cendpetra@gmail.com
 */
class EventsAdapter(
    private val itemListener: (Event) -> Unit
) : ListAdapter<Event, EventsAdapter.EventViewHolder>(DIFF_UTIL) {

    private companion object {
        private val DIFF_UTIL = object : DiffUtil.ItemCallback<Event>() {
            override fun areItemsTheSame(
                oldItem: Event,
                newItem: Event
            ) = oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: Event,
                newItem: Event
            ) = oldItem == newItem
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): EventViewHolder  = EventViewHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_event,
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class EventViewHolder(
        private val views: ItemEventBinding
    ) : RecyclerView.ViewHolder(views.root) {

        fun bind(event: Event) {
            with(views) {
                this.event = event
                root.setOnClickListener {
                    itemListener.invoke(event)
                }
                executePendingBindings()
            }
        }
    }
}