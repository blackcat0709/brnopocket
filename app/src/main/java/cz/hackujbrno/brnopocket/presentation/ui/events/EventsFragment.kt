package cz.hackujbrno.brnopocket.presentation.ui.events

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import cz.hackujbrno.brnopocket.databinding.FragmentEventsBinding
import cz.hackujbrno.brnopocket.presentation.extensions.observe
import cz.hackujbrno.brnopocket.presentation.ui.events.EventsViewModel.EventsState
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Created by Petra Cendelínová on 09,April,2022
 * cendpetra@gmail.com
 */
class EventsFragment : Fragment() {

    private lateinit var viewBinding: FragmentEventsBinding

    private val eventsViewModel: EventsViewModel by viewModel()

    private val eventsAdapter: EventsAdapter by lazy {
        EventsAdapter {
            val bundle = bundleOf("event" to it)

            startActivity(Intent(requireContext(), EventDetailActivity::class.java).apply {
                putExtras(bundle)
            })
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentEventsBinding.inflate(layoutInflater)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observe(eventsViewModel.eventsState) {
            observeEventsState(it)
        }

        setupViews()
        eventsViewModel.getEvents()
    }

    private fun setupViews() {
        with(viewBinding) {
            events.adapter = eventsAdapter
        }
    }

    private fun observeEventsState(eventsState: EventsState) {
        when (eventsState) {
            EventsState.Error -> {
                // todo error
            }
            is EventsState.Loaded -> eventsAdapter.submitList(eventsState.events)
            EventsState.Loading -> {
                // todo
            }
        }

    }
}