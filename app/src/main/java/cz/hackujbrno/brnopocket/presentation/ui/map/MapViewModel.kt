package cz.hackujbrno.brnopocket.presentation.ui.map

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.hackujbrno.brnopocket.domain.models.WasteYard
import cz.hackujbrno.brnopocket.domain.usecases.GetWasteYardsUseCase
import kotlinx.coroutines.launch

/**
 * Created by Petra Cendelínová on 10,April,2022
 * cendpetra@gmail.com
 */
class MapViewModel(
    private val getWasteYards: GetWasteYardsUseCase
) : ViewModel() {

    private val _wasteYardsState: MutableLiveData<WasteYardsSte> = MutableLiveData()
    val wasteYardsState: LiveData<WasteYardsSte> = _wasteYardsState

    fun getWasteYards() {
        viewModelScope.launch {
            getWasteYards.execute().onSuccess {
                _wasteYardsState.postValue(WasteYardsSte.Loaded(it))
            }.onFailure {
                _wasteYardsState.postValue(WasteYardsSte.Error)
            }
        }
    }

    sealed class WasteYardsSte {
        object Loading : WasteYardsSte()
        data class Loaded(val yards: List<WasteYard>) : WasteYardsSte()
        object Error : WasteYardsSte()
    }
}