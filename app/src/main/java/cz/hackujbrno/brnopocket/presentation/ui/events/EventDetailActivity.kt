package cz.hackujbrno.brnopocket.presentation.ui.events

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import cz.hackujbrno.brnopocket.R
import cz.hackujbrno.brnopocket.databinding.ActivityEventDetailBinding
import cz.hackujbrno.brnopocket.domain.models.Event
import java.util.*

class EventDetailActivity: AppCompatActivity() {

    private lateinit var viewBinding: ActivityEventDetailBinding
    private var event: Event? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        event = intent.extras?.getParcelable<Event>("event")

        viewBinding = (DataBindingUtil.setContentView<ActivityEventDetailBinding?>(
            this,
            R.layout.activity_event_detail
        ).apply {
            this.event = this@EventDetailActivity.event
        })

        setupViews()

    }

    private fun setupViews() {
        with(viewBinding) {
            crossIcon.setOnClickListener {
                finish()
            }

            navigateButton.setOnClickListener {

                event?.run {
                    val uri: String =
                        java.lang.String.format(
                            Locale.ENGLISH,
                            "geo:%f,%f",
                            latitude,
                            longitude
                        )
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                    startActivity(intent)
                }

            }

            urlButton.setOnClickListener {

            }
        }
    }

}