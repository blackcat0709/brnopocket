package cz.hackujbrno.brnopocket.presentation

import android.text.Html
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import coil.load
import com.google.android.material.textview.MaterialTextView
import cz.hackujbrno.brnopocket.domain.models.Event
import cz.hackujbrno.brnopocket.presentation.extensions.decodeAddress
import cz.hackujbrno.brnopocket.presentation.helpers.TimeHelper


@BindingAdapter("decodeAddress")
fun MaterialTextView.decodeAddress(event: Event) {
    text = context.decodeAddress(event.latitude, event.longitude)
}

@BindingAdapter("formatDateTime")
fun MaterialTextView.formatDateTime(date: Long) {
    text = TimeHelper.formatDateTime(date)
}

@BindingAdapter("setHtml")
fun MaterialTextView.setHtml(name: String) {
    text = Html.fromHtml(name, Html.FROM_HTML_MODE_COMPACT);
}

@BindingAdapter("loadImage")
fun ImageView.loadImage(url: String) {
    load(url)
}

