package cz.hackujbrno.brnopocket.presentation.customViews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import cz.hackujbrno.brnopocket.R
import cz.hackujbrno.brnopocket.databinding.ViewNotificationTypeBinding
import timber.log.Timber

/**
 * Created by Petra Cendelínová on 09,April,2022
 * cendpetra@gmail.com
 */
class NotificationTypeView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val views = ViewNotificationTypeBinding.inflate(
        LayoutInflater.from(context),
        this,
        true
    )

    init {
        attrs?.let {
            val typedArray =
                context.obtainStyledAttributes(it, R.styleable.NotificationTypeView)
            try {
                with(views) {
                    title.text = typedArray.getString(R.styleable.NotificationTypeView_titleRes)
                    icNotification.setImageDrawable(
                        typedArray.getDrawable(R.styleable.NotificationTypeView_iconRes)
                    )
                }
            } catch (e: Exception) {
                Timber.w("Failed setup attrs at: $e")
            }
            typedArray.recycle()
        }

    }
}