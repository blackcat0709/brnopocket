package cz.hackujbrno.brnopocket.presentation.ui.events

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.hackujbrno.brnopocket.domain.models.Event
import cz.hackujbrno.brnopocket.domain.usecases.GetEventsUseCase
import kotlinx.coroutines.launch

/**
 * Created by Petra Cendelínová on 09,April,2022
 * cendpetra@gmail.com
 */
class EventsViewModel(
    private val getEvents: GetEventsUseCase
) : ViewModel() {

    private val _eventsState: MutableLiveData<EventsState> = MutableLiveData()
    val eventsState: LiveData<EventsState> = _eventsState

    fun getEvents() {
        viewModelScope.launch {
            getEvents.execute().onSuccess {
                _eventsState.postValue(EventsState.Loaded(it))
            }.onFailure {
                _eventsState.postValue(EventsState.Error)
            }
        }
    }

    sealed class EventsState {
        object Loading : EventsState()
        data class Loaded(val events: List<Event>) : EventsState()
        object Error : EventsState()
    }

}