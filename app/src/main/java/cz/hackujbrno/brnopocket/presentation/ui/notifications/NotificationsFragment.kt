package cz.hackujbrno.brnopocket.presentation.ui.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import cz.hackujbrno.brnopocket.databinding.FragmentNotificationsBinding

/**
 * Created by Petra Cendelínová on 09,April,2022
 * cendpetra@gmail.com
 */
class NotificationsFragment: Fragment() {

    private lateinit var viewBinding: FragmentNotificationsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentNotificationsBinding.inflate(layoutInflater)

        return viewBinding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
//        viewBinding.unbind()
    }
}