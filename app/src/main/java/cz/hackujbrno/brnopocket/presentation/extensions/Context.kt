package cz.hackujbrno.brnopocket.presentation.extensions

import android.content.Context
import android.location.Address
import android.location.Geocoder
import java.util.*


/**
 * Created by Petra Cendelínová on 10,April,2022
 * cendpetra@gmail.com
 */

fun Context.decodeAddress(lat: Double, long: Double): String {
    val geocoder = Geocoder(this, Locale.getDefault())
    val addresses: List<Address> = geocoder.getFromLocation(lat, long, 1)
    return addresses[0].getAddressLine(0)
}