package cz.hackujbrno.brnopocket.data.repo

import cz.hackujbrno.brnopocket.data.remote.services.WasteYardsApi
import cz.hackujbrno.brnopocket.domain.models.WasteYard
import cz.hackujbrno.brnopocket.domain.repo.PlacesRepositoryApi
import timber.log.Timber

/**
 * Created by Petra Cendelínová on 10,April,2022
 * cendpetra@gmail.com
 */
class PlacesRepositoryImpl(
    private val wasteYardsApi: WasteYardsApi
) : PlacesRepositoryApi {

    override suspend fun getWasteYards(): Result<List<WasteYard>> = try {
        val wasteYards = wasteYardsApi.getWasteYards().features.map {
            WasteYard(
                id = it.attributes.id,
                name = it.attributes.name,
                x = it.geometry.x,
                y = it.geometry.y
            )
        }

        Result.success(wasteYards)
    } catch (e: Exception) {
        Timber.e("Failed to get waste yards: $e")
        Result.failure(e)
    }

}