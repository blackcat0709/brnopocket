package cz.hackujbrno.brnopocket.data.remote.models.wasteYards

import com.google.gson.annotations.SerializedName

/**
 * Created by Petra Cendelínová on 10,April,2022
 * cendpetra@gmail.com
 */
data class WasteYardResponse(
    @SerializedName("objectid")
    val id: Long,
    @SerializedName("nazev_sso")
    val name: String,
)