package cz.hackujbrno.brnopocket.data.repo

import cz.hackujbrno.brnopocket.data.remote.services.EventsApi
import cz.hackujbrno.brnopocket.domain.mappers.toEvent
import cz.hackujbrno.brnopocket.domain.models.Event
import cz.hackujbrno.brnopocket.domain.repo.CultureRepositoryApi
import timber.log.Timber

/**
 * Created by Petra Cendelínová on 09,April,2022
 * cendpetra@gmail.com
 */
class CultureRepositoryImpl(
    private val eventsApi: EventsApi
) : CultureRepositoryApi {

    override suspend fun getEvents(): Result<List<Event>> = try {
        val events = eventsApi.getEvents().features.map { it.attributes.toEvent() }
        Result.success(events)
    } catch (e: Exception) {
        Timber.e("Failed to get events: $e")
        Result.failure(e)
    }
}