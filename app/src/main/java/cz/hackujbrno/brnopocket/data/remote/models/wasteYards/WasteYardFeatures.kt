package cz.hackujbrno.brnopocket.data.remote.models.wasteYards

/**
 * Created by Petra Cendelínová on 10,April,2022
 * cendpetra@gmail.com
 */
class WasteYardFeatures(
    val features: List<WasteYardAttributes>
)