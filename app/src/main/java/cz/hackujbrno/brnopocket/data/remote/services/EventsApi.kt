package cz.hackujbrno.brnopocket.data.remote.services

import cz.hackujbrno.brnopocket.data.remote.models.events.EventsFeatures
import retrofit2.http.GET

/**
 * Created by Petra Cendelínová on 09,April,2022
 * cendpetra@gmail.com
 */
interface EventsApi {

    @GET("Events/FeatureServer/0/query?where=1%3D1&outFields=*&outSR=4326&f=json")
    suspend fun getEvents(): EventsFeatures
}