package cz.hackujbrno.brnopocket.data.remote.models.events

import com.google.gson.annotations.SerializedName

/**
 * Created by Petra Cendelínová on 09,April,2022
 * cendpetra@gmail.com
 */
data class EventResponse(
    @SerializedName("ID")
    val id: Long,
    val name: String,
    @SerializedName("text")
    val description: String?,
    val tickets: String?,
    @SerializedName("tickets_info")
    val ticketsInfo: String?,
    @SerializedName("first_image")
    val imageUrl: String,
    val url: String,
    val categories: String?,
    @SerializedName("organizer_email")
    val organizerEmail: String?,
    @SerializedName("date_from")
    val dateFrom: Long,
    @SerializedName("date_to")
    val dateTo: Long,
    val latitude: Double,
    val longitude: Double
)