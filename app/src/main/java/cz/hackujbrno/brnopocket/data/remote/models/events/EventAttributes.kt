package cz.hackujbrno.brnopocket.data.remote.models.events

/**
 * Created by Petra Cendelínová on 09,April,2022
 * cendpetra@gmail.com
 */
data class EventAttributes(
    val attributes: EventResponse
)