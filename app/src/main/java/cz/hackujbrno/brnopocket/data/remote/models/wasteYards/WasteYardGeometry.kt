package cz.hackujbrno.brnopocket.data.remote.models.wasteYards

/**
 * Created by Petra Cendelínová on 10,April,2022
 * cendpetra@gmail.com
 */
data class WasteYardGeometry(
    val x: Double,
    val y: Double
)