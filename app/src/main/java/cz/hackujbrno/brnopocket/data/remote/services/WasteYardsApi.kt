package cz.hackujbrno.brnopocket.data.remote.services

import cz.hackujbrno.brnopocket.data.remote.models.wasteYards.WasteYardFeatures
import retrofit2.http.GET

/**
 * Created by Petra Cendelínová on 10,April,2022
 * cendpetra@gmail.com
 */
interface WasteYardsApi {

    @GET("sberna_strediska_odpadu/FeatureServer/0/query?where=1%3D1&outFields=*&outSR=4326&f=json")
    suspend fun getWasteYards(): WasteYardFeatures
}