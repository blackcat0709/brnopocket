package cz.hackujbrno.brnopocket.data.remote.models.wasteYards

/**
 * Created by Petra Cendelínová on 10,April,2022
 * cendpetra@gmail.com
 */
data class WasteYardAttributes(
    val attributes: WasteYardResponse,
    val geometry: WasteYardGeometry
)