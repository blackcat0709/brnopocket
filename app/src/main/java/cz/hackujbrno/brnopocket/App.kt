package cz.hackujbrno.brnopocket

import android.app.Application
import io.kommunicate.Kommunicate
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Created by Petra Cendelínová on 09,April,2022
 * cendpetra@gmail.com
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()

        Kommunicate.init(applicationContext, getString(R.string.chatbot_api_key))

        startKoin {
            androidContext(applicationContext)
            Kommunicate.init(applicationContext, getString(R.string.chatbot_api_key));
            modules(
                listOf(
                    viewModelsModule(),
                    domainModule(),
                    dataModule()
                )
            )
        }
    }
}